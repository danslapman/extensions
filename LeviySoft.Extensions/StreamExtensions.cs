﻿using System.IO;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for Stream
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// Reads the whole stream to byte array
        /// </summary>
        /// <param name="input">Stream instance</param>
        /// <returns>Stream contents in array</returns>
        public static byte[] ToArray(this Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
