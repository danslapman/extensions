﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for string
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Indicates that string is not Empty
        /// This method is npe-safe
        /// </summary>
        /// <param name="str">string instance</param>
        /// <returns>Yes/no</returns>
        public static bool IsNotEmpty(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }

        /// <summary>
        /// Indicates that string is Empty
        /// This method is npe-safe
        /// </summary>
        /// <param name="str">string instance</param>
        /// <returns>Yes/no</returns>
        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        /// <summary>
        /// Replaces whole string using dictionary
        /// </summary>
        /// <param name="str">string instance</param>
        /// <param name="replacements">Replacement variants</param>
        /// <returns>Resulting string</returns>
        public static string Replace(this string str, IDictionary<string, string> replacements)
        {
            return replacements.ContainsKey(str) ? replacements[str] : str;
        }

        /// <summary>
        /// Replaces all variant occurrences
        /// </summary>
        /// <param name="str">string instance</param>
        /// <param name="replacements">Replacement variants</param>
        /// <returns>Resulting string</returns>
        public static string ReplaceAll(this string str, IDictionary<string, string> replacements)
        {
            return replacements.Aggregate(str,
                (current, replacement) => current.Replace(replacement.Key, replacement.Value));
        }

        /// <summary>
        /// Capitalizes string
        /// </summary>
        /// <param name="str">string instance</param>
        /// <returns>Capitalized string</returns>
        public static string Capitalize(this string str)
        {
            return char.ToUpper(str[0]) + ((str.Length > 1) ? str.Substring(1).ToLower() : string.Empty);
        }

        /// <summary>
        /// Converts HEX string to byte array
        /// </summary>
        /// <param name="str">string instance</param>
        /// <returns>Array of bytes</returns>
        public static byte[] ToByteArrayHex(this string str)
        {
            return Enumerable.Range(0, str.Length)
                .Where(x => x%2 == 0)
                .Select(x => Convert.ToByte(str.Substring(x, 2), 16))
                .ToArray();
        }
    }
}
