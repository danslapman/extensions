﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for IEnumerable
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Applies IEnumerable instance
        /// WARNING! This method can cause multiple enumeration
        /// </summary>
        /// <typeparam name="T">IEnumerable type</typeparam>
        /// <param name="enumerable">Instance</param>
        /// <param name="action">Action</param>
        /// <returns>Enumerated instance</returns>
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public static IEnumerable<T> EnumerateWith<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var elem in enumerable)
            {
                action(elem);
            }
            return enumerable;
        }
    }
}
