﻿using System.Collections.Generic;
using System.Linq;

namespace LeviySoft.Extensions
{
    /// <summary>
    /// Extension methods for IDictionary
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Gets value using specified key. If key does not exist in dictionary instance, returns defaultValue
        /// </summary>
        /// <param name="d">IDictionary instance</param>
        /// <param name="key">Key</param>
        /// <param name="defaultValue">Default value to return</param>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TVal">Value type</typeparam>
        /// <returns>TVal instance</returns>
        public static TVal Get<TKey, TVal>(this IDictionary<TKey, TVal> d, TKey key, TVal defaultValue=default(TVal))
        {
            return d.ContainsKey(key) ? d[key] : defaultValue;
        }


        /// <summary>
        /// Determines whether the IDictionary contains an element for each of specified keys
        /// </summary>
        /// <param name="d">IDictionary instance</param>
        /// <param name="keys">Key set</param>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TVal">Value type</typeparam>
        /// <returns>Weighted conclusion</returns>
        public static bool ContainsKeys<TKey, TVal>(this IDictionary<TKey, TVal> d, params TKey[] keys)
        {
            return keys.All(d.ContainsKey);
        }
    }
}
