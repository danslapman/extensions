﻿using System;
using System.Linq.Expressions;
using Patterns.Containers;
using Shouldly;
using Xunit;


namespace LeviySoft.Extensions.Tests
{
    public class TestContainer
    {
        public string TestField;
        public string TestProperty { get; set; }
        public TestContainer InnerContainer { get; set; }
    }

    public class ExpressionsExtensionsTests
    {
        private static string StringExtractor<T>(Expression<Func<T, bool>> expression)
        {
            var binaryExpression = expression.Body as BinaryExpression;
            return binaryExpression != null ? (string)binaryExpression.Right.ExtractValue() : null;
        }

        private static TRes GenericExtractor<T, TRes>(Expression<Func<T, bool>> expression)
            where TRes : class 
        {
            var binaryExpression = expression.Body as BinaryExpression;
            return binaryExpression != null ? (TRes)binaryExpression.Right.ExtractValue() : null;
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        private class Demo
        {
            public Demo(string id, int intProperty, Option<string> stringProperty)
            {
                Id = id;
                IntProperty = intProperty;
                StringProperty = stringProperty;
            }

            public string Id { get; private set; }
            public int IntProperty { get; private set; }
            public Option<string> StringProperty { get; private set; } 
        }

        private string _testField;
        private string TestProperty { get; set; }

        [Fact]
        public void ShouldGetValueFromConstant()
        {
            StringExtractor<Demo>(d => d.Id == "1").ShouldBe("1");
        }

        [Fact]
        public void ShouldGetOptionalValueFromInstance()
        {
            GenericExtractor<Demo, Option<string>>(d => d.StringProperty == new Some<string>("ololo")).ShouldBe<Option<string>>(new Some<string>("ololo"));
        }

        [Fact]
        public void ShouldGetValueFromVariable()
        {
            // ReSharper disable once ConvertToConstant.Local
            var a = "1";
            StringExtractor<Demo>(d => d.Id == a).ShouldBe("1");
        }

        [Fact]
        public void ShouldGetValueFromField()
        {
            _testField = "1";
            StringExtractor<Demo>(d => d.Id == _testField).ShouldBe("1");
        }

        [Fact]
        public void ShouldGetValueFromProperty()
        {
            TestProperty = "1";
            StringExtractor<Demo>(d => d.Id == TestProperty).ShouldBe("1");
        }

        [Fact]
        public void ShouldGetValueFromInnerField()
        {
            var container = new TestContainer {TestField = "1"};
            StringExtractor<Demo>(d => d.Id == container.TestField).ShouldBe("1");
        }

        [Fact]
        public void ShouldGetValueFromInnerProperty()
        {
            var container = new TestContainer {TestProperty = "1"};
            StringExtractor<Demo>(d => d.Id == container.TestProperty).ShouldBe("1");
        }

        [Fact]
        public void ShouldGetValueFromDoubleInnerField()
        {
            var container = new TestContainer {InnerContainer = new TestContainer {TestField = "1"}};
            StringExtractor<Demo>(d => d.Id == container.InnerContainer.TestField).ShouldBe("1");
        }

        [Fact]
        public void ShouldGetValueFromDoubleInnerProperty()
        {
            var container = new TestContainer { InnerContainer = new TestContainer { TestProperty = "1" } };

            StringExtractor<Demo>(d => d.Id == container.InnerContainer.TestProperty).ShouldBe("1");
        }

        [Fact]
        public void ShouldGetValueFromMethodInvokation()
        {
            var v = StringExtractor<Demo>(d => d.Id == string.Format("{0}", 1));
            StringExtractor<Demo>(d => d.Id == string.Format("{0}", 1)).ShouldBe("1");
        }
    }
}
