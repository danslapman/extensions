﻿using Xunit;

namespace LeviySoft.Extensions.Tests.TypeExtensionsTests
{
    interface IBase { }
    interface IBase<T> : IBase { }
    interface ISuccessor<T> : IBase<T> { }
    interface IFinally<T> : ISuccessor<T> { }
    class BaseCollection<T> : IBase<T> { }

    public class ImplementsInterfaceSpecificTests
    {
        [Fact]
        public void ImplementsInterfaceDetectsBaseInterfaces()
        {
            Assert.True(typeof(IBase<>).ImplementsInterface(typeof(IBase)));
        }

        [Fact]
        public void ImplementsInterfaceDetectsBaseInterfacesInTypizedInterfaces()
        {
            Assert.True(typeof(IBase<string>).ImplementsInterface(typeof(IBase)));
        }

        [Fact]
        public void ImplementsInterfaceDetectsBaseInterfacesInClasses()
        {
            Assert.True(typeof(BaseCollection<>).ImplementsInterface(typeof(IBase)));
        }

        [Fact]
        public void ImplementsInterfaceDetectsBaseInterfaceInTypizedClasses()
        {
            Assert.True(typeof(BaseCollection<string>).ImplementsInterface(typeof(IBase)));
        }

        [Fact]
        public void TypizedGenericInterfaceShouldImplementInself()
        {
            Assert.True(typeof(IBase<string>).ImplementsInterface(typeof(IBase<>)));
        }
    }
}
